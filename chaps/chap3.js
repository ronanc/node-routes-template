// jshint node:true
'use strict';

function main(num) {
    console.log('\nchap' + num);

    // console.log(power(2, 10));

    // console.log(findSolution(23822));

    // console.log(min(3, 7));

    // console.log(isEven(-1));

    console.log(countBs('Blue Banana'));
    // console.log(countChar('Blue Banana', 'a'));
    
}

function countBs(str) {
    return countChar(str, 'B');
}

function countChar(str, char) {
    var count = 0;
    for (var i = 0; i < str.length; i++) {
        if (str.charAt(i) === char)
            count++;
    }

    return count;
}

function isEven(num) {
    if (num < 0) {
        return null;
    }

    if (num === 0) {
        return true;
    } else if (num === 1) {
        return false;
    } else {
        return isEven(num - 2);
    }
}

function min(a, b) {
    if (a < b) {
        return a;
    } else {
        return b;
    }
}

function findSolution(target) {
    function find(start, history) {
        if (start === target) {
            return history;
        } else if (start > target) {
            return null;
        } else {
            return find(start + 5, '(' + history + ' + 5)') ||
                find(start * 3, '(' + history + ' * 3)');
        }
    }
    return find(1, '1');
}

// recursive function
function power(base, exponent) {
    if (exponent === 0) {
        return 1;
    } else {
        return base * power(base, exponent - 1);
    }
}

function closure() {
    var twice = multiplier(2);
    console.log(twice(5));

    var tripler = multiplier(3);
    console.log(tripler(5));
}

function optionalArgs() {
    console.log('R', 2, 'D', 2);
}

function callStack() {
    console.log(chicken() + ' came first.');
}

function chicken() {
    return egg();
}

function egg() {
    return chicken();
}

// this is a factory
function multiplier(factor) {
    return function (number) {
        return number * factor;
    };
}


module.exports = main;