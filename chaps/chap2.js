// jshint node:true
'use strict';

function main(num) {
    console.log('\nchap' + num);
    triLoop();
    fizzBuzz();
    chessBoard();
}

function triLoop() {
    var count = 0;
    for (var i = 0; i < 7; i++) {
        count++;
        for (var j = 0; j < count; j++) {
            process.stdout.write('#');
        }
        console.log();
    }
}

function fizzBuzz() {
    for (var i = 1; i <= 100; i++) {
        if (i % 3 === 0 && i % 5 === 0) {
            console.log('FizzBuzz');
        } else if (i % 3 === 0) {
            console.log('Fizz');
        } else if (i % 5 === 0) {
            console.log('Buzz');
        }

        console.log(i);
    }
}

function chessBoard() {
    var size = 8,
        even,
        odd;

    for (var i = 0; i < size; i++) {
        if (i % 2 === 0) {
            even = '#';
            odd = ' ';
        } else {
            even = ' ';
            odd = '#';
        }

        for (var j = 0; j < size; j++) {
            if (j % 2 === 0) {
                process.stdout.write(even);
            }
            else {
                process.stdout.write(odd);
            }
        }
        console.log();
    }
}

module.exports = main;