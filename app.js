// jshint node:true
'use strict';

var app = require('express')();
var port = 8080;
var chapNum = 3;

app.use('/', require('./routes'));

app.listen(port, function () {
    console.log('listening on port:', port);

    require('./chaps/chap' + chapNum)(chapNum);
});