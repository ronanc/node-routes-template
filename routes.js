// jshint node:true
'use strict';

var router = require('express').Router();

router.get('/', home);
router.get('/chap/:id', chap);

module.exports = router;

//////////////

// middleware for this router (this is a catch all function)
router.use(function timeLog(req, res, next) {
    console.log('Time: ', Date.now());
    next();
});

function home(req, res) {
    res.status(200).send('home');
}

function chap(req, res) {
    var chapNum = req.params.id;

    try {
        require('./chaps/chap' + chapNum)(chapNum);
    } catch (error) {
        console.log(error);
    }

    res.status(200).send('chap' + chapNum);
}